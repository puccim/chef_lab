# # -*- mode: ruby -*-
# # vi: set ft=ruby :

PROJECT_NAME="chef_lab"
DOMAIN_NAME="fuffa.net"
vagrant_args = ARGV

Vagrant.configure("2") do |config|

  # lambda block to sync public key on each node
  # sync_key_lambda= lambda do |s,user="root"|

  #   # Create the ssh-key on the vm provisioned if it does not exists
  #   system 'ssh-keygen -t rsa -b  -P "" -q' unless File.exist? "#{Dir.home}/.ssh/id_rsa.pub"
    
  #   ssh_pub_key = File.readlines("#{Dir.home}/.ssh/id_rsa.pub").first.strip

  #   home_dir="/root"
  #   home_dir="/home/"+user unless user.eql?"root"

  #   s.inline = <<-SHELL
  #       echo #{ssh_pub_key} >> #{home_dir}/.ssh/authorized_keys
  #       sort -u #{home_dir}/.ssh/authorized_keys -o #{home_dir}/.ssh/authorized_keys
  #   SHELL
      
  # end

  # lambda block to sync hostname resolution
  sync_hosts_lambda= lambda do |s|

    puts "sync_hosts_lambda"

  s.inline=<<-SHELL

       echo "192.168.60.2 chef.#{DOMAIN_NAME} chef">>/etc/hosts
       echo '192.168.60.3 work.#{DOMAIN_NAME} work'>> /etc/hosts
       echo '192.168.60.4 node01.#{DOMAIN_NAME} node01'>>/etc/hosts
       echo '192.168.60.5 node02.#{DOMAIN_NAME} node02'>>/etc/hosts
       echo '192.168.60.6 node03.#{DOMAIN_NAME} node03'>> /etc/hosts

       sort -u /etc/hosts -o /etc/hosts && sort -n /etc/hosts -o /etc/hosts

  SHELL

  end if ( vagrant_args.include? "hosts-sync" and vagrant_args.include? "provision" )
  
  
  config.vm.provision "shell", inline: "echo Start Provisioning #{PROJECT_NAME} ... "

  ############################### Chef Server -  ubuntu 16.04 ###############################

  config.vm.define :chef , primary: true do |node|
    node.vm.box = "ubuntu/xenial64"
    node.vm.hostname = "chef.#{DOMAIN_NAME}"
    node.vm.provider "virtualbox" do |vb|
      vb.name = PROJECT_NAME+"_chef"
      vb.memory=2048
    end

    # NOTE
    # eth0 as NAT (10.0.2.15 is the same for each VM) is a fundamental requirement of Vagrant in its current state.
    # 

    # Host only private network on 192.168.60.*
    node.vm.network "private_network", ip: "192.168.60.2"
    
    Dir.mkdir "./shared/chef" unless File.exists? "./shared/chef"
    node.vm.synced_folder "./shared/chef", "/vagrant"

    # Create a forwarded port mapping which allows access to a specific port
    # within the machine from a port on the host machine. In the example below,
    # accessing "localhost:3000" will access port 3000 on the guest machine.
    # node.vm.network "forwarded_port", guest: "3000", host: "3000", host_ip: "127.0.0.1", auto_correct: true

    node.vm.provision "up_and_running" , type: "shell" do |s|
      s.inline = "echo Chef Node Up and Running..."
    end
    
    node.vm.provision "update" , type: "shell", run: "never" do |s|
      s.inline = "echo Updating $(hostname)... ; apt-get update && apt-get -y upgrade; "
    end

    # Vagrant installation at up and provision phase
    node.vm.provision "rvm", type: "shell", path: "scripts/install-rvm.sh", args: "stable", privileged: false, run: "never"
    node.vm.provision "ruby-24", type: "shell", path: "scripts/install-ruby.sh", args: "2.4 pry rails", privileged: false, run: "never"

    # Ruby interactive setup
    node.vm.provision "ruby-environment", type: "shell", privileged: false, run: "never" do |s|

        print "Installing new ruby, specify version[Valid are: 1.9.3 , 2.0, 2.2, 2.4]:"
        version = STDIN.gets.chomp
        print "\n"
        s.args= [version]

        print "Gems required[Valid are: rake rails ]:"
        gems = STDIN.gets.chomp
        print "\n"
        s.args << gems if gems.strip.length>0
        
        s.path = "scripts/ruby-environment.sh"
          
    end if ( vagrant_args.include? "ruby-environment" and vagrant_args.include? "provision" )

    # https://docs.chef.io/install_dk.html
    node.vm.provision "chef-environment" , type: "shell", run: "never" do |s|
      
      # s.inline= "curl -L https://omnitruck.chef.io/install.sh | sudo bash"

      s.inline= "wget --quiet https://packages.chef.io/files/stable/chef-server/12.17.15/ubuntu/16.04/chef-server-core_12.17.15-1_amd64.deb;"
      s.inline+="dpkg -i chef-server-core_12.17.15-1_amd64.deb;"
      s.inline+="rm chef-server-core_12.17.15-1_amd64.deb;"
      # s.inline+="chef-server-ctl reconfigure;"

      # Install ntp daemon on debian and on centos
      # apt-get install ntp # | ntpd on centos
      # systemctl start ntp # | ntpd on centos
      # systemctl enable ntp # | ntpd on centos

      # Install console and reconfigure
      # chef-server-ctl install chef-manage
      # chef-server-ctl reconfigure
      # chef-manage-ctl reconfigure --accept-license
      
      # Create organization and user
      # cd /root/
      # mkdir .chef      
      # chef-server-ctl org-create fuffa "Fuffanet Inc." -f /root/.chef/fuffa.pem
      # chef-server-ctl user-create calife Sanchez Gutierrez califerno@gmail.com 1234hpNET -f /root/.chef/calife.pem
      # chef-server-ctl user-create admin Chef Administrator chef@yourdomain.com 1234hpNET -f ~/.chef/admin.pem
      # chef-server-ctl org-user-add -a fuffa calife
      # chef-server-ctl org-user-add -a fuffa admin
      # cp .chef/calife.pem .chef/fuffa.pem .chef/admin.pem /vagrant/

      # Open browser
      # open url https://192.168.60.2/organizations/fuffa

    end

    # ssh-key exchange for automatic logic, ssh login disabled by service (sshd_config)
    # node.vm.provision "ssh-sync" ,  type: "shell" , run: "never" do |s|
    #   # sync_key_lambda.call(s) # root user
    #   # sync_key_lambda.call(s,"ubuntu") # ubuntu user
    # end

    node.vm.provision "hosts-sync" ,  type: "shell" , run: "never" do |s|
      sync_hosts_lambda.call(s)
    end
  
  end

  ############################### CHEF WORKSTATION  ###############################

  config.vm.define :work do |node|
    node.vm.box = "ubuntu/xenial64"    
    node.vm.hostname="work.#{DOMAIN_NAME}"
    node.vm.provider "virtualbox" do |vb|
      vb.name = PROJECT_NAME+"_work"
      vb.memory=768
    end
    node.vm.network "private_network", ip: "192.168.60.3"
    Dir.mkdir "./shared/work" unless File.exists? "./shared/work"
    node.vm.synced_folder "./shared/work", "/vagrant"

    node.vm.provision "shell", inline: "echo WORK Node Up and Running..."

    node.vm.provision "update" , type: "shell", run: "never" do |s|
      s.inline = "echo Updating $(hostname)... ; apt-get update && apt-get -y upgrade; "
    end
    
    node.vm.provision "chef-environment" , type: "shell", run: "never" do |s|
      s.inline= "wget --quiet https://packages.chef.io/files/stable/chefdk/2.4.17/ubuntu/16.04/chefdk_2.4.17-1_amd64.deb; 
                dpkg -i chefdk_2.4.17-1_amd64.deb; 
                rm chefdk_2.4.17-1_amd64.deb "

      # Give it a try
      # curl https://omnitruck.chef.io/install.sh | sudo bash -s -- -nP chefdk -c stable -v 2.0.28
      
      # Create workspace restaurant
      # cd $HOME/ && mkdir restaurant/ && cd restaurant;
      # git init .
      # mkdir .chef
      # echo '.chef' >> ~/restaurant/.gitignore

      # Client configuration
      
      # vi .chef/knife.rb      
      # current_dir = File.dirname(__FILE__)
      # log_level                :info
      # log_location             STDOUT
      # node_name                'calife'
      # client_key               "#{current_dir}/calife.pem"
      # #validation_client_name   'fuffa-validator'
      # #validation_key           "#{current_dir}/fuffa.crt"
      # validation_key           "/nonexists"
      # chef_server_url          'https://chef.fuffa.net/organizations/fuffa'
      # cache_type               'BasicFile'
      # cache_options( :path => "#{ENV['HOME']}/.chef/checksums" )
      # cookbook_path            ["#{current_dir}/../cookbooks"]

      # knife ssl fetch

      # verify with
      # knife ssl check
      # knife node list
      #
      #copy calife.pem (private from server to .chef folder)

      #
      # Bootstrap nodes from workstation, inside cookbook folder
      # vagrant up node{01,02,03} and syncronize key on each nodes
      # 
      #
      # Validatorless Bootstrap - Install chef-client on node via ominubus installer
      # $HOME/restaurant
      # knife bootstrap node01.fuffa.net --server-url https://chef.fuffa.net/organizations/fuffa --node-name NODE01 --identity-file ~/.ssh/id_rsa -xvagrant --sudo
      # knife bootstrap node02.fuffa.net --server-url https://chef.fuffa.net/organizations/fuffa --node-name NODE02 --identity-file ~/.ssh/id_rsa -xvagrant --sudo
      # knife bootstrap node03.fuffa.net --server-url https://chef.fuffa.net/organizations/fuffa --node-name NODE03 --identity-file ~/.ssh/id_rsa -xvagrant --sudo

      # Create and upload cookbook
      # cd $HOME/restaurant
      # chef generate cookbook good_food
      # knife cookbook upload

      # Cookbook repos
      # workspace clone from https://gitlab.com/puccim/restaurant.git
      # git clone https://gitlab.com/puccim/restaurant.git
      
    end

    # ssh-key exchange for automatic logic, ssh login disabled by service (sshd_config)
    # node.vm.provision "ssh-sync" ,  type: "shell", run:"never" do |s|
    #   # sync_key_lambda.call(s,"ubuntu") # ubuntu user
    # end

    node.vm.provision "hosts-sync" ,  type: "shell", run:"never" do |s|
      sync_hosts_lambda.call(s)
    end
    
  end

  ############################### Node01 - ubuntu 12.04 ###############################

  config.vm.define :node01 do |node|
    node.vm.box = "ubuntu/xenial64"
    node.vm.hostname = "node01.#{DOMAIN_NAME}"
    node.vm.provider "virtualbox" do |vb|
      vb.name = PROJECT_NAME+"_node01"
      vb.memory=512
    end

    node.vm.network "private_network", ip: "192.168.60.4"
    Dir.mkdir "./shared/node01" unless File.exists? "./shared/node01"
    node.vm.synced_folder "./shared/node01", "/vagrant"
    node.vm.provision "shell", inline: "echo Node01 Node Up and Running..."

    node.vm.provision "update" , type: "shell" , run: "never" do |s|
#      s.inline = "echo Updating $(hostname)... ; DEBIAN_FRONTEND=noninteractive && apt-get update && apt-get -y -o Dpkg::Options::=\"--force-confdef\" -o Dpkg::Options::=\"--force-confold\" upgrade "
      s.inline = "echo Updating $(hostname)... ; apt-get update && apt-get -y upgrade; "
    end

    # node.vm.provision "ssh-sync" ,  type: "shell", run:"never" do |s|
    #   sync_key_lambda.call(s)
    # end

    node.vm.provision "hosts-sync" ,  type: "shell", run:"never" do |s|
      sync_hosts_lambda.call(s)
    end

  end

  ############################### CENTOS6 ###############################
  
  config.vm.define :node02 do |node|
    node.vm.box = "centos/6"
    node.vm.hostname="node02.#{DOMAIN_NAME}"
    # node.vm.box_version = "1707.01"
    node.vm.provider "virtualbox" do |vb|
      vb.name = PROJECT_NAME+"_node02"
      vb.memory=512
    end
    node.vm.network "private_network", ip: "192.168.60.5"
    Dir.mkdir "./shared/node02" unless File.exists? "./shared/node02"
    node.vm.synced_folder "./shared/node02", "/vagrant"
    
    node.vm.provision "shell", inline: "echo NODE02 Node Up and Running..."

    node.vm.provision "shell", inline: "echo \"Bugfix to enabled sync on Centos OS\"; ln -sf /usr/lib/x86_64-linux-gnu/VBoxGuestAdditions/mount.vboxsf /sbin/mount.vboxsf", run: "always"

    node.vm.provision "update" , type: "shell", run: "never" do |s|
      s.inline = "echo Updating $(hostname)... ; yum update -y; "
    end

    # node.vm.provision "ssh-sync" ,  type: "shell", run:"never" do |s|
    #   sync_key_lambda.call(s)
    # end
        
    node.vm.provision "hosts-sync" ,  type: "shell",run: "never" do |s|
      sync_hosts_lambda.call(s)
    end
    
  end

  ############################### Node03 - ubuntu 12.04 ###############################

  #
  # knife configure initial
  #

  # vi .chef/knife.rb
  # node_name                'calife'
  # client_key               '/root/.chef/calife.pem'
  # # validation_client_name   'fuffa-validator'
  # validation_key           '/etc/chef/client.pem'
  # chef_server_url          'https://CHEF/organizations/fuffa'
  # syntax_check_cache_path  '/root/.chef/syntax_check_cache'

  # ssh-keygen -t rsa

  # mkdir /etc/chef

  # knife ssl fetch # --> trusted_certs/...crt
  # knife ssl check

  # vi /root/.chef/calife.pem # certificato user calife pem (private key) , generato da chef-server-ctl sul server

  # knife client create -e vi NODE03 -f /etc/chef/client.pem

  # knife client list

  # knife node create NODE01 -e vi -V

  config.vm.define :node03 do |node|
    node.vm.box = "ubuntu/xenial64"
    node.vm.hostname = "node03.#{DOMAIN_NAME}"
    node.vm.provider "virtualbox" do |vb|
      vb.name = PROJECT_NAME+"_node03"
      vb.memory=512
    end
    node.vm.network "private_network", ip: "192.168.60.6"
    Dir.mkdir "./shared/node03" unless File.exists? "./shared/node03"
    node.vm.synced_folder "./shared/node03", "/vagrant"
    node.vm.provision "shell", inline: "echo Node03 Node Up and Running..."

    node.vm.provision "update" , type: "shell", run: "never" do |s|
#      s.inline = "echo Updating $(hostname)... ; DEBIAN_FRONTEND=noninteractive && apt-get update && apt-get -y -o Dpkg::Options::=\"--force-confdef\" -o Dpkg::Options::=\"--force-confold\" upgrade "
      s.inline = "echo Updating $(hostname)... ; apt-get update && apt-get -y upgrade; "
    end

    # node.vm.provision "ssh-sync" ,  type: "shell",run: "never" do |s|
    #   sync_key_lambda.call(s)
    # end
    
    node.vm.provision "hosts-sync" ,  type: "shell",run:"never" do |s|
      sync_hosts_lambda.call(s)
    end
    

  end

end
