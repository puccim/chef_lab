#!/usr/bin/env bash

# Install rvm stable version

# gpg --keyserver hkp://keys.gnupg.net --recv-keys 409B6B1796C275462A1703113804BB82D39DC0E3 7D2BAF1CF37B13E2069D6956105BD0E739499BDB

#gpg2 --recv-keys 409B6B1796C275462A1703113804BB82D39DC0E3

curl -sSL https://rvm.io/mpapis.asc | gpg --import -

curl -sSL --insecure https://get.rvm.io | bash -s stable

if [ $? -eq 0 ]; then
	
	# Install ruby version provided in input as first parameterx

	source $HOME/.rvm/scripts/rvm || source /etc/profile.d/rvm.sh	

	rvm --install $1

	rvm use --default $1

	# Install gems

	shift

	if (( $# ))
	then gem install $@
	fi

	rvm cleanup all

	echo "mkdir source folder...$HOME/source/ " ; mkdir -p $HOME/source/

	echo "Install required packages to run rails..." ; sudo apt-get install -y nodejs libpq-dev
	
else
	echo "Command failed..."
	
fi;
