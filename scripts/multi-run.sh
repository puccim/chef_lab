#!/bin/bash
#
# 1) Parallel execution of $COMMAND on nodes vms-li-01,vms-li-02
# ./multi-run.sh -svms-li-01,vms-li-02 -ptrue
#
# 2) Sequential execution of $COMMAND on nodes vms-li-01,vms-li-02
# ./multi-run.sh -svms-li-01,vms-li-02
#
# 17.01.2018 base version
# Changelog
# 2018-01-18 checkResolveNodes , checkSSHLoginEnabledOnNodes
#

###  MAIN ###

### Start getopts code ###

#Initialize variables to default values.
SERVERS=""
PARALLEL="false"
COMMAND="hostname; sleep 5; date;"

##### Start Check Function #####

# verify that SERVERS is a valid server list
function checkResolveNodes() {
	local _nodes=$(echo "$1" | tr "," "\n")

	for _node in $_nodes
	do

		if ! ping -c 1 $_node >/dev/null 2>&1
		then
			echo "[ERROR] Node $_node is not reachable"
			return 1
		fi

	done
	return 0;
}

# verify that root user can authenticate without password
function checkSSHLoginEnabledOnNodes() {
	local _nodes=$(echo "$1" | tr "," "\n")

	for _node in $_nodes
	do
		if ! ssh -o PasswordAuthentication=no  -o BatchMode=yes root@$_node exit > /dev/null 2>&1
		then
			echo "[ERROR] SSH Root Login on node $_node not enabled without password"
			return 1
		fi
		
	done
	
	return 0;
}

##### End Check Function #####

##### Start Parameters parser #####
while getopts "s:p:h" FLAG; do
    case $FLAG in
        s)
            SERVERS=$OPTARG
            ;;
        p)
            if [[ $OPTARG =~ ^true|false|TRUE|FALSE$ ]]; then
                PARALLEL=$OPTARG
            else
                echo "$OPTARG Is not a number, exit 2"
                exit 2
            fi
            ;;
        h) #show help
            echo "Usage: $(basename $0) -svms-li-01,vms-li-01 -ptrue - [-s:servers , -p:parallel]"
            exit 2
            ;;
        \?) #unrecognized option - show help
            echo "option $OPTARG not valid, exit 2"
            echo "Usage: $(basename $0) -svms-li-01,vms-li-01 -ptrue - [-s:servers , -p:parallel]"
            exit 2
            ;;
    esac
done

shift $((OPTIND-1))  #This tells getopts to move on to the next argument.

##### End Parameters parser #####

echo "=================================="
echo "Servers: " $SERVERS
echo "Parallel: " $PARALLEL
echo "=================================="

if checkResolveNodes $SERVERS -eq 0 ; then echo "Check 'All Nodes valid': PASSED"; else echo "Check 'All Nodes valid': NOT PASSED"; echo "[$(date +%T)] $(basename $0) - Run completed with errors"; exit 3; fi;

if checkSSHLoginEnabledOnNodes $SERVERS -eq 0 ; then echo "Check 'SSH login enabled': PASSED"; else echo "Check 'SSH login enabled': NOT PASSED"; echo "[$(date +%T)] $(basename $0) - Run completed with errors"; exit 4; fi;


nodes=$(echo $SERVERS | tr "," "\n")

for node in $nodes
do

        if [[ $PARALLEL =~ ^true|TRUE$ ]]; then
                echo "[$(date +%T)] Running parallel script on node [$node], via ssh"
                ssh root@$node "$COMMAND" 2>&1 | { while read line; do echo "[$node][$(date +%H:%M:%S)] $line"; done; } | tee /tmp/multi-run-"$node"-$(date +%T).log &
        else
                echo "[$(date +%T)] Running script on node [$node], via ssh"
                ssh root@$node " $COMMAND " 2>&1  | { while read line; do echo "[$node][$(date +%H:%M:%S)] $line"; done; } | tee /tmp/multi-run-"$node"-$(date +%T).log
    fi

done

wait

echo "[$(date +%T)] $(basename $0) - Run successfully completed"
